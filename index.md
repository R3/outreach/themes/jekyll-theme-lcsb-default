---
---

# LCSB theme for [Jekyll](https://jekyllrb.com/)
This repository contains a template you should use when creating any Jekyll-based web page for your LCSB-related projects. 

Jekyll is a *static webpages generator*, which converts markdown files to a website. Coupled with Gitlab-CI, it can generate and host the pages for you in a completely automatic way (based on your commits to Gitlab).

 > The layout **has been already approved** by our Communications Department - you don't need to spend time on web design nor asking for additional approvals.

# 1. How to use it?

## How to start a new Jekyll page using our theme?...
The easiest way to start a new Jekyll page is to fork [this repository (containing an empty example page)](https://gitlab.lcsb.uni.lu/core-services/pages-jekyll-lcsb-template).

The webpage will be published automatically under uni.lu network once you fork the repository - you don't need to open any tickets.

After you have forked the repository, you should fill it with your content (either using Gitlab IDE, or cloning the repository and using your favourite Markdown editor) and push the changes using Git.

For more detailed information on all the steps and a guide, please refer to [https://gitlab.lcsb.uni.lu/core-services/pages-jekyll-lcsb-template/-/blob/master/guide.md](https://gitlab.lcsb.uni.lu/core-services/pages-jekyll-lcsb-template/-/blob/master/guide.md).

## ... or how to use the LCSB theme in your existing Jekyll page?
1. Put the following lines in your project's Gemfile: 
    ```
    gem 'jekyll-theme-lcsb-default', '~> 0.4.7'

    gem 'jekyll-theme-lcsb-frozen-components', 
        '~> 0.0.2', 
        :git => "https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-frozen-components.git", 
        :tag => "0.0.2"
    ```
2. Specify the theme in your project's `_config.yml` file:
    ```
    theme: jekyll-theme-lcsb-default
    plugins:
      - jekyll-theme-lcsb-frozen-components
    ```
3. Run `bundle install` to fetch the theme.
4. You're all set!

> Please note, that unless you want to work directly on the theme, you should not use/clone this repository (`jekyll-theme-lcsb-default`) but use `pages-jekyll-lcsb-template` instead.

# 2. Theme development
Please check a separate guide about developing and maintaining the theme, which is available under: [https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default/blob/master/development.md](https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default/blob/master/development.md).

## Branches
 * **latest** contains latest version
 * **master** contains old, legacy version - it might be used by some old projects

## Contributing
Bug reports and pull requests are welcome on Gitlab at [https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default/issues](https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default/issues). 

[Please click here to open a new issue](https://gitlab.lcsb.uni.lu/core-services/jekyll-theme-lcsb-default/-/issues/new?issue).

This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Contributors
The people actively engaged in the development are:
 * Jacek Lebioda ([jacek.lebioda@uni.lu](mailto:jacek.lebioda@uni.lu))
 * Laurent Heirendt
 * Yohan Jarosz

We've accepted contributions from:
 * Christophe Trefois
 * Elisabeth Guerard
 * Mirek Kratochvil

# 3. Contact
In case you need any information or help, please don't hesitate to contact [LCSB Sysadmins Team (lcsb-sysadmins@uni.lu)](mailto:lcsb-sysadmins@uni.lu) or the active developers.
# 4. License
The theme is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
