---
layout: default
title: development
show_bookmark_button: true
order: 3
---

## Please note
 > This instructions are intended for developers of the theme, and **not the developers of the website that use the theme**!

# Plugins
Plugins which are placed in `_plugins` directory **are not published automatically with the theme!**. 

In order to publish a new one, add it to [https://gitlab.lcsb.uni.lu/-/ide/project/core-services/jekyll-theme-lcsb-frozen-components](https://gitlab.lcsb.uni.lu/-/ide/project/core-services/jekyll-theme-lcsb-frozen-components).

# Banners
The templates for banners are available in our OwnCloud: <https://owncloud.lcsb.uni.lu/apps/files/?dir=/BioCore/logos/R3/git-r3lab/Gitlab-pages&fileid=9451143>

# Development
To set up your environment to develop this theme, run `bundle install`.

Your theme is setup just like a normal Jekyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, `_sass` and `assets` tracked with Git will be bundled.
To add a custom directory to your theme-gem, please edit the regexp in `jekyll-theme-lcsb-default.gemspec` accordingly.

# Automatic gem publishing
Gitlab CI has two special triggers when it comes to publishing:
 * if you change the contents of `lib/jekyll-theme-lcsb-default.rb` file, it will be automatically tagged
 * whenever the commit is tagged, Gitlab CI automatically builds and publishes a new version of theme to Rubygems

For details refer to [the documentation of ruby gems](https://guides.rubygems.org/make-your-own-gem/), and this repository's `.gitlab-ci.yml` and settings.

# "Hidden" features

## Share link in the footer (used by Howto-cards)
Set `shortcut` in the file header, and `share_url` in `_config.html`. Take a look in `_includes/footer.html` to see how the links are generated in the footer.
## "Print this page" button
Set `show_print_button: true` in the file header (or in the default settings in `_config.yml`).

## Where to add additional scripts or stylesheets to the page?
There are two files that can be replaced in the website - `_includes/css-imports.html` and `_includes/scripts.html`.

## Social media icons
Take a look in `_includes/social.html`.

## Banner link
If you want to override the link on the banner (by default it points to your index page), use the following in `_config.yml`:
`banner_link: https://wwwen.uni.lu/`

## Add to bookmarks
If you want to see "Add to bookmarks" button, set: `bookmarks_enabled: true` in `_config.yml`, and set `show_bookmark_button: true` in the file header (or in the default settings in `_config.yml`).
